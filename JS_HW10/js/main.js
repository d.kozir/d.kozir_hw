
const form = document.querySelector('form')
form.addEventListener('click', e => {
    let target = e.target
    if (target.tagName !== 'I') return
    if (target.classList.contains('fa-eye')) {
        target.classList.replace('fa-eye', 'fa-eye-slash')
        target.previousElementSibling.type = 'text'
    }else if (target.classList.contains('fa-eye-slash')){
        target.classList.replace('fa-eye-slash', 'fa-eye')
        target.previousElementSibling.type = 'password'
    }
})

const btn = document.querySelector('button')

btn.addEventListener('click', event => {
    event.preventDefault()
    const passwordValue = document.getElementById('password').value
    const confirmPasswordValue = document.getElementById('confirm_password').value

    if (passwordValue !== confirmPasswordValue){
        let secondInput = document.querySelector('.second-input')
        secondInput.after(error)
    }
    else
        alert('You are welcome')
})

const error = document.createElement('p')
error.innerHTML = 'Нужно ввести одинаковые значения'
error.style.color = 'red'


