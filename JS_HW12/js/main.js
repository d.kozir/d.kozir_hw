
let show = true;
const delay = 3000;
let timer;
const arrayImages = ["img/image1.jpg", "img/image2.jpg", "img/image3.jpg", "img/image4.jpg"];
let activeIndex = 0;

function nextImage() {
document.getElementById("img_main").src = arrayImages[activeIndex];
if (show) {
        timer = setTimeout(() => {
                if (activeIndex === arrayImages.length -1) {
                        activeIndex = 0;
                } else {
                        activeIndex += 1;
                }
                nextImage();
                }, delay);
} else {
        clearTimeout(timer);
}
}

function start() {
        if (!show) {
                show = true;
                nextImage();
        }
}


function stop() {
        if (show) {
                show = false;
                clearTimeout(timer);
        }
}

nextImage();