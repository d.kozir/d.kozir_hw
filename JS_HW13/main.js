

const style = document.querySelector('#main_css')
document.querySelector('.btn-change-theme').addEventListener('click', function (){


    if (localStorage.getItem('main_2')) {
        localStorage.removeItem('main_2')
        style.href = "./css/main.css";
    }
    else {
        localStorage.setItem('main_2', "./css/main_2.css")
        style.href = "./css/main_2.css";
    }
})

window.addEventListener('DOMContentLoaded', () => {
    if (localStorage.getItem('main_2')){
        style.href = "./css/main_2.css"
    }
})