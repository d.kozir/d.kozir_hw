
$("#navbar").on("click","a", function (event) {
    event.preventDefault();
    let id  = $(this).attr('href');
    let top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1000);
});

$("#hot-news").click(function(){
    $("main div.hot-news-list").slideToggle(1000);
    return false;
});

const scroll = $('#scroll-up');
$(window).scroll(function() {
    if ($(window).scrollTop() > 400) {
        scroll.addClass('show');
    } else {
        scroll.removeClass('show');
    }
});

scroll.on('click', function() {
    $('html, body').animate({scrollTop:0}, '2000');
});

