// Описать своими словами в несколько строчек, зачем в программировании нужны циклы.

//Ответ:
//     Если возникает необходимость в повторе каких-либо действий - создают цикл.
//     Примеры циклов в жизни: выкладывание продуктов в холодильник, мытье посуды, пока она не закончится.


let number = +prompt('Please, enter a number');

while (!Number.isInteger(number)) {
    number = +prompt('Wrong, please, try again');
}

if (number < 5) {
    console.log('Sorry, no numbers');
}

for (let i = 0; i < number; i++) {
    if (i % 5 === 0) {
        console.log(i);
    }
}


let m = +prompt('num1');
let n = +prompt('num2');
while (m > n) {
    console.log('Mistake!');
    m = +prompt('num1');
    n = +prompt('num2');
}
nextStep: for (let i = m; i < n; i++) {
    for (let j = m; j < i; j++) {
        if (i % j === 0) {
            continue nextStep;
        }
    }
    console.log(i)
}