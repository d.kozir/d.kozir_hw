
    // 1. Описать своими словами для чего вообще нужны функции в программировании.
    // 2. Описать своими словами, зачем в функцию передавать аргумент.
    //
    //     Функцию создают для решения конкретных задач и облегчения работы. Чтобы не писать одну и ту же последовательность кода для решения
    //     однотипных задач, создают функцию один раз и, при необходимости, просто вызывают ее в любой момент.
    //     Зачем в функцию переадвать аргумент?
    //     Если не указывать аргументы - функция просто не будет знать, что ей вообще что-то передано,
    //     т.е. не будет понимать, с какой входной информацией работать.

function OperationResult (a, b, action) {
    switch (action) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a / b;
    }
}

let Number1 = +prompt('Please, enter number 1');

while (isNaN(Number1)) {
    Number1 = prompt('Wrong, enter a number correctly');
}

let Number2 = +prompt('Please, enter number 2');

while (isNaN(Number2)) {
    Number2 = prompt('Wrong, enter a number correctly');
}

let Operation = prompt('Please, enter one of these operations: +, -, *, /');

while (
    Operation !== '+' &&
    Operation !== '-' &&
    Operation !== '*' &&
    Operation !== '/') {
    Operation = prompt('Please, enter correct!');
}

console.log(OperationResult(Number1, Number2, Operation));



