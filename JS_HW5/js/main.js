// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
// Если необходимо использовать спец символ как обычный, нужно поставить перед ним \.
// Это и называется экранированием.

function createNewUser() {

    return {
        firstName: prompt('What is your name?'),
        lastName: prompt('What is your last name?'),
        birthday: prompt('Enter, please, your date of birth in a format: dd.mm.yyyy'),
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            let now = new Date();
            let nowYear = now.getFullYear();
            let date = +this.birthday.substring(0, 2);
            let month = +this.birthday.substring(3, 5);
            let year = +this.birthday.substring(6, 10);
            let birthDate = new Date(year, month-1, date);
            let birthYear = birthDate.getFullYear();
            let age = nowYear - birthYear;
            if (now < new Date(birthDate.setFullYear(nowYear))) {
               age = age-1;
            }
            return age;
        },
        getPassword(){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6, 10);
        }
    }
}

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

