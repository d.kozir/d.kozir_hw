// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
// это программный интерфейс, c помощью которого мы можем получить доступ к содержимому html, изменять содержимое и структуру.



const createList = (arr, parent = document.body) => {
    const listItemsDOM = arr.map (element => `<li>${element}</li>`).join("");
    return parent.insertAdjacentHTML('afterbegin', listItemsDOM)
}

let springArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
createList(springArray)










