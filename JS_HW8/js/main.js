
let input = document.querySelector('input')

let textPrice = document.createElement('span')
textPrice.classList.add('span_style')
document.body.prepend(textPrice)

let error = document.createElement('div')
error.classList.add('error_style')
document.body.append(error)

input.addEventListener('focus', function (){
    input.style.borderColor = 'green'
    if (input.value < 0) {
        error.innerHTML = ''
        input.value = ''
        textPrice.value = ''
    }
})

input.addEventListener('blur', function (){
    input.style.borderColor = ''
    if (input.value < 0) {
        input.style.borderColor = 'red'
        error.innerHTML = 'Please enter correct price'
        error.style.color = 'red'
    }
    if (input.value !== '' && input.value >= 0) {
        textPrice.textContent = `Текущая цена: ${input.value}`
        textPrice.insertAdjacentHTML("beforeend", '<button>x</button>')
        input.style.color = 'green'
    }
})

textPrice.onclick = function() {
    textPrice.remove()
    input.value = ''
}



