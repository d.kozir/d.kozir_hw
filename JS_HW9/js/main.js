
const tab = document.querySelectorAll('.list_filter')

tab.forEach(function(el) {
    el.addEventListener('click', function() {
        const id = this.getAttribute('data-filter'),
            content = document.querySelector('.list_filter_wrapper[data-filter="'+id+'"]'),
            activeTab = document.querySelector('.tab-el.active'),
            activeContent = document.querySelector('.tab-content.active');

        activeTab.classList.remove('active'); 
        el.classList.add('active');
        activeContent.classList.remove('active');
        content.classList.add('active');
    });
});
