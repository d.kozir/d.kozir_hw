document.getElementById("root").innerHTML = "<ul id='list'></ul>"

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function  correctCheck({author, name, price}, i) {
    if (!author) {
        throw new Error(`Book ${i + 1}. No author.`);
    }
    if (!name) {
        throw new Error(`Book ${i + 1}. No name.`);
    }
    if (!price) {
        throw new Error(`Book ${i + 1}. No price.`);
    }
}

books.forEach((el, i) => {
    try {
        correctCheck(el, i);
        document.getElementById('list').innerHTML += `<li>Book ${i + 1}. Author: ${el.author} Name: ${el.name} Price: ${el.price}</li>`
    } catch (error) {
        console.log(error.message);
    }
});


