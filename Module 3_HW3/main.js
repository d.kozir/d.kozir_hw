class Employee {
    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }
    get name(){
        return this._name
    }
    set name(anotherName){
        this._name = anotherName
    }
    get age(){
        return this._age
    }
    set age(anotherAge){
        this._age = anotherAge
    }
    get salary(){
        return this._salary
    }
    set salary(anotherSalary){
        this._salary = anotherSalary
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this._lang = lang
    }
    get salary(){
        return this._salary * 3
    }
    set salary(anotherSalary){
        this._salary = anotherSalary
    }
    get lang(){
        return this._lang
    }
    set lang(anotherLang){
        this._lang = anotherLang
    }
}

let employee = new Programmer('Sasha', 30, 2500, ['english', 'russian', 'french'])
let employee2 = new Programmer('Dasha', 25, 3000, ['ukrainian', 'english', 'french'])

console.log(employee)
console.log(employee2)
