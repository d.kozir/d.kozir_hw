
let requestURL = 'https://ajax.test-danit.com/api/swapi/films'
let movies = document.getElementById('movies')


function showFilm(url, parent) {
    fetch(url)
        .then((response) => response.json())
        .then((data) => {
            data.map((films) => {
                parent.insertAdjacentHTML('beforebegin', `<h3 class="title">Episode ${films.episodeId} - ${films.name}</h3>
                 <ul class='filmCharacter${films.id}'></ul>
                 <p>${films.openingCrawl}</p>`)

                for (let i = 0; i < films.characters.length; i++) {
                    fetch(films.characters[i])
                        .then(response => response.json())
                        .then((char) => {
                            const ul = document.querySelector(`.filmCharacter${films.id}`)
                            ul.insertAdjacentHTML('beforebegin', `<li>${char.name}</li>`)
                        })
                }
            })
        })
}
showFilm(requestURL, movies)
