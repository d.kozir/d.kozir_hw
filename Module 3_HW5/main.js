const button = document.getElementById('btn')
const container = document.getElementById('container')
const IpUrl = 'https://api.ipify.org/?format=json'

const ipReceive = async ()=>{
    const response = await fetch(IpUrl)
    const {ip} = await response.json()
    return ip
}

const userInfo = async ip=>{
    const address = await fetch(`https://ip-api.com/json/${ip}`)
    const addressInfo = await address.json()
    return addressInfo
}

button.addEventListener('click', async () => {
    const ipNumber = await ipReceive()
    const data = await userInfo(ipNumber)
    const {continent, country, region, city, district} = data
    const ul = document.createElement('ul')
    ul.innerHTML= `<li>Continent: ${continent}</li>
                    <li>Country: ${country}</li>
                    <li>Region: ${region}</li>
                    <li>City: ${city}</li>
                    <li>District: ${district}</li>`

    container.append(ul)
})