const burger = document.querySelector('.header_burger')
const navList = document.querySelector('.nav-list')

document.addEventListener('click', function (){
    burger.classList.toggle('active')
    navList.classList.toggle('active')
})
