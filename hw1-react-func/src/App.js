import React, { useState } from 'react';
import Header from "./components/Header";
import Modals from "./components/Modals";

const App = () => {
    const [showModal, setShowModal] = useState(false)
    const [modals, setModals] = useState([])

    const modalsObj = [
        {
            id: 'modalID1',
            title: 'Do you want to delete this file?',
            description: 'Once you delete this file, it will not be possible to undo this session. Are you sure you want to delete it?',
        },
        {
            id: 'modalID2',
            title: 'Modal INFO',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt vero illo error eveniet cum',
        },
    ]

    const openModal = ({target}) => {
        const modalID = modalsObj.find(item => item.id === target.id)
        setModals([modalID])
        setShowModal(true)
    }

    return (
        <div className="container" >
            <Header title='React App' onClick={openModal} />
            {showModal && <Modals modals={modals} closeModal={() => setShowModal(!showModal)} />}
        </div>
  );


}

export default App;
