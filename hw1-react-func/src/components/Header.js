import React from "react";
import PropTypes from 'prop-types';
import Button from "./Button";

const Header = ({ title, onClick }) => {

    return (
        <header className='header' >
            <h1>{title}</h1>
            <Button id='modalID1' color='#e91212' text='Open first modal' onClick={onClick}/>
            <Button id='modalID2' color='#0d75c3' text='Open second modal' onClick={onClick} />
        </header>
    );
};

Header.defaultProps = {
    title: 'React Application'
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
}

export default Header;