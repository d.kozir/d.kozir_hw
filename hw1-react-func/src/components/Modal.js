import React  from 'react';
import {  FaTimes } from 'react-icons/fa';
import Button from "./Button";

const Modal = ({ modal, closeModal }) => {

    return (
        <div className='modal-wrapper' onClick={closeModal} >
            <div className={modal.id === 'modalID1' ? 'modal modal1' : 'modal modal2'} >
                <h3>{modal.title} <FaTimes style={{ color: 'white', cursor: 'pointer' }} onClick={closeModal} /></h3>
                <p>{modal.description}</p>
                <Button color={modal.id === 'modalID1' ? '#ac0f0f' : '#0f3d6e'} text='Ok' />
                <Button color={modal.id === 'modalID1' ? '#ac0f0f' : '#0f3d6e'} text='Cancel' onClick={closeModal}/>
            </div>
        </div>
    );
};


export default Modal;
