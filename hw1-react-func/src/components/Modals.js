import React from 'react';
import Modal from "./Modal";

const Modals = ({ modals, closeModal  }) => {

    return (
        <>
            {modals.map((modal) => (
                <Modal key={modal.id} modal={modal} closeModal={closeModal}/>
            ))}
        </>
    );
};

export default Modals;