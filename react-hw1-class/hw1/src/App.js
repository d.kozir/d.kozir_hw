import React from 'react';
import './index.scss';
import Modal from "./components/Modal";
import Button from "./components/Button";

const modalsObj = [
    {
        id: 'modalID1',
        title: 'Do you want to delete this file?',
        description: 'Once you delete this file, it will not be possible to undo this session. Are you sure you want to delete it?',
    },
    {
        id: 'modalID2',
        title: 'Modal INFO',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt vero illo error eveniet cum',
    },
]

class App extends React.Component {
    state = {
        show: false,
        modalObj: []
    };

    showModal = e => {
        this.setState({
            show: !this.state.show
        });
    };

    openModal = ({target}) => {
        const modalID = modalsObj.find(item => item.id === target.id)
        this.setState({modalObj: modalID});
        this.setState({show: true})
    }

    render() {
        return (
                <>
                    <Button
                        id='modalID1'
                        className="btn"
                        text="Open first modal"
                        backgroundColor="red"
                        onClick={this.openModal}/>
                    <Button
                        id='modalID2'
                        className="btn"
                        text="Open second modal"
                        backgroundColor="blue"
                        onClick={this.openModal}/>

                    {this.state.show && <Modal key={this.state.modalObj.id} modal={this.state.modalObj} onClose={this.showModal} show={this.state.show} />}
                </>
        );
    };

}

export default App;