import React from 'react';
import PropTypes from "prop-types";
import {  FaTimes } from 'react-icons/fa';
import Button from "./Button";


export default class Modal extends React.Component {
    onClose = e => {
        this.props.onClose && this.props.onClose(e);
    };
    render() {
        console.log(this.props)
        if (!this.props.show) {
            return null;
        }
        return (
            <div className='modal-wrapper' onClick={this.onClose} >
                <div className={this.props.modal.id === 'modalID1' ? 'modal modal1' : 'modal modal2'} >
                    <h3>{this.props.modal.title}<FaTimes style={{ color: 'white', cursor: 'pointer' }} onClick={this.onClose} /></h3>
                    <p>{this.props.modal.description}</p>
                    <Button
                        className="btn"
                        text="Ok"
                        backgroundColor="red"
                    />
                    <Button
                        className="btn"
                        text="Close"
                        backgroundColor="blue"
                        onClick={this.onClose}
                    />
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    onClose: PropTypes.func.isRequired
}