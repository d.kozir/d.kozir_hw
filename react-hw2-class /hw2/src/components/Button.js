import React from 'react';
import PropTypes from 'prop-types';

class Button extends React.Component {
    render() {
        const {props} = this
        const {backgroundColor, className, onClick, text, id } = props
        const styles = {
            backgroundColor: backgroundColor
        }

        return (
            <button
                id={id}
                className = {className}
                onClick = {onClick}
                style={styles}>
                {text}
            </button>
        )
    }

}


Button.defaultProps = {
    color: 'blue'
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    onclick: PropTypes.func,
    className: PropTypes.string.isRequired,
}

export default Button;