import React from 'react';
import Modal from "./Modal";
import {FaStar} from 'react-icons/fa';
import Button from "./Button";


const modalsObj = [
    {
        id: 'modalID1',
        title: 'Add to cart',
        description: 'Do you really want to add this trip to cart?',
    },
    {
        id: 'modalID2',
        title: 'Modal INFO',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt vero illo error eveniet cum',
    },
]

class Product extends React.Component {
    state = {
        show: false,
        modalObj: [],
        isActive: true
    };

    showModal = e => {
        this.setState({
            show: !this.state.show
        });
    };

    openModal = ({target}) => {
        const modalID = modalsObj.find(item => item.id === target.id)
        this.setState({modalObj: modalID});
        this.setState({show: true})
    }

    addToFavorite() {
        this.setState({isActive: !this.state.isActive});
        const favItem = this.props.item;
        localStorage.setItem("Trip", JSON.stringify(favItem));
    };

    addToBin() {
        const favItem = this.props.item;
        localStorage.setItem("TripBin", JSON.stringify(favItem));
    };

    render() {
        const {article, title, src, price, item, addToFav, removeFromFav} = this.props;


        return (
            <>
                <section className='section'>
                    <img src={src} alt=""/>
                    <div style={{position: 'relative'}}>
                        <p className='title'>{title}</p>
                        <p className='price'>Price: {price}</p>
                        <p className='article'>Article: {article}</p>
                        {this.state.isActive && <FaStar onClick={() => {
                            this.addToFavorite();
                            addToFav(item)
                        }} className="icon_star white_star"/>}

                        {!this.state.isActive && <FaStar onClick={() => {
                            this.addToFavorite();
                            removeFromFav(item)
                        }} className="icon_star orange_star"/>}
                        <Button
                            id='modalID1'
                            className="btn"
                            text="Add to cart"
                            backgroundColor="red"
                            onClick={this.openModal}/>

                        {this.state.show && <Modal key={this.state.modalObj.id}
                                                   modal={this.state.modalObj}
                                                   addToBin={this.addToBin.bind(this)}
                                                   onClose={this.showModal}
                                                   show={this.state.show} />}
                    </div>
                </section>
            </>
        )
    }
}

export default Product;