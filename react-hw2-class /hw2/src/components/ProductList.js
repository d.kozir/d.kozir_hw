
import React from 'react';
import Product from "../components/Product";

export class ProductList extends React.Component {
    state = {
        data: [],
        favItems: [],
    };

    componentDidMount() {
        fetch('./products.json')
            .then(response => response.json())
            .then(data => {
                this.setState({data: data});
            })
    };

    addToFav = item => {
        this.setState(prevState => ({
            favItems: [...prevState.favItems, item]
        }));
    };

    removeFromFav = item => {
        const index = item.id;
        const list = this.state.favItems;
        this.setState(() => {
            const favItems = list.filter(item => item.id !== index);
            return {
                favItems
            };
        });
        localStorage.removeItem("Trip")
    };

    render() {
        return (
            <section className = 'product_container'>
                {this.state.data.map(item =>
                    <Product
                        title={item.name}
                        key={item.id}
                        article={item.article}
                        item={item}
                        src={item.img}
                        price={item.price}
                        addToFav={this.addToFav}
                        removeFromFav={this.removeFromFav}
                    />)
                }
            </section>
        )
    }
}

export default ProductList;