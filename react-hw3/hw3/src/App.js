import React from 'react';
import { Link, BrowserRouter as Router } from 'react-router-dom';
import AppRoutes from "./components/AppRoutes";

const App = () => {
        return (
            <>
                <Router>
                    <nav className='header_nav'>
                        <ul className='nav_list'>
                            <li className='nav_item'>
                                <Link to='/' className='nav_item_link'>Home</Link>
                            </li>
                            <li className='nav_item'>
                                <Link to='/favourite' className='nav_item_link'>Favourite</Link>
                            </li>
                            <li className='nav_item'>
                                <Link to='/bin' className='nav_item_link'>Bin</Link>
                            </li>
                        </ul>
                    </nav>
                    <AppRoutes />
                </Router>
            </>
        )
}
export default App;

