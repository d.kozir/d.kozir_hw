import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from "./Pages/Home";
import Favourite from "./Pages/Favourite";
import Bin from "./Pages/Bin";
import NotFound from "./Pages/NotFound";

const AppRoutes = () => {
    return (
        <Switch>
            <Route exact path='/'>
                <Home  />
            </Route>
            <Route path='/favourite'>
                <Favourite   />
            </Route>
            <Route path='/bin'>
                <Bin />
            </Route>
            <Route component={NotFound}/>
        </Switch>
    );
};

export default AppRoutes;