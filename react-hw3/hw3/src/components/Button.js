import React from 'react';
import PropTypes from 'prop-types';


const Button = ({ backgroundColor, className, onClick, text, id }) => {

    return (
        <button
            id={id}
            className = {className}
            onClick = {onClick}
            style= {{ backgroundColor: backgroundColor }}>
            {text}
        </button>
    )

}

Button.defaultProps = {
    color: 'blue'
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    onclick: PropTypes.func,
    className: PropTypes.string.isRequired,
}

export default Button;