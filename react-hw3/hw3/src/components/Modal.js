import React from 'react';
import PropTypes from "prop-types";
import {  FaTimes } from 'react-icons/fa';
import Button from "./Button";

const Modal = ( { modal, addToBin, onClose, show} ) => {

    const close = e => {
        onClose && onClose(e);
    }

    if (!show) {
        return null;
    }

    return (
        <div className='modal-wrapper' onClick={close} >
            <div className={modal.id === 'modalID1' ? 'modal modal1' : 'modal modal2'} >
                <h3>{modal.title}<FaTimes style={{ color: 'white', cursor: 'pointer' }} onClick={close} /></h3>
                <p>{modal.description}</p>
                <Button
                    className="btn"
                    text="Ok"
                    backgroundColor="red"
                    onClick={addToBin}
                />
                <Button
                    className="btn"
                    text="Close"
                    backgroundColor="blue"
                    onClick={close}
                />
            </div>
        </div>
    )
}

Modal.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
}

export default Modal;