import React, {useState, useEffect} from 'react'
import {FaTimes} from "react-icons/fa";
import ProductList from "../ProductList";

const Bin = () => {
    const [binItems, setBinItems] = useState([])

    useEffect(() => {
        const bin = JSON.parse(localStorage.getItem('TripBin')) || []
        setBinItems(bin)
    }, [])

    const removeFromBin = card => {
        const index = binItems.findIndex(item => item.id === card.id)
        binItems.splice(index, 1)
        localStorage.setItem('TripBin', JSON.stringify([...binItems]))
        setBinItems([...binItems])
    }

    if (binItems.length === 0) {
        return <p style={{ textAlign: 'center' }}>NO PRODUCTS ADDED</p>
    }

    const close = <FaTimes id='modalID2' style={{ color: 'black', cursor: 'pointer' }} onClick={removeFromBin} />

    return (
        <section className='product_container'>
            <ProductList allList={binItems} close={close}/>
        </section>
    )
}
export default Bin;
