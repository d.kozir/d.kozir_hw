import React, {useEffect, useState} from 'react';
import ProductList from "../ProductList";

const Favourite = ( ) => {
    const [favItem, setFavItem] = useState([])

    useEffect(() => {
        const fav = JSON.parse(localStorage.getItem('Trip')) || []
        setFavItem(fav)
    }, [favItem])


    if (favItem.length === 0) {
        return <p style={{ textAlign: 'center' }}>NO PRODUCTS ADDED</p>
    }

    return (
       <section className='product_container'>
           <ProductList allList={favItem}  />
       </section>
    )
}
export default Favourite;
