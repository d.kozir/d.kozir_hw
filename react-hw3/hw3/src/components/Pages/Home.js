import React, {useState, useEffect} from 'react';
import ProductList from "../ProductList";

const Home = () => {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [allItems, setAllItems] = useState([])

    useEffect(() => {
        fetch('./products.json')
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setAllItems(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    if (error) {
        return <div>Ошибка: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>LOADING...</div>;
    } else {
        return (
            <>
                <ProductList allList={allItems}/>
            </>
        );
    }
};

export default Home;