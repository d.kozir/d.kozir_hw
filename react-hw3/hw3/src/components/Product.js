import React, {useState, useEffect} from 'react';
import Modal from "./Modal";
import {FaStar} from 'react-icons/fa';
import Button from "./Button";

const modalsObj = [
    {
        id: 'modalID1',
        title: 'Add to cart',
        description: 'Do you really want to add this trip to cart?',
    },
    {
        id: 'modalID2',
        title: 'Modal INFO',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt vero illo error eveniet cum',
    },
]

const Product = ( { src, price, article, title, addToFav, id, addToBin, close}) => {
    const [isActive, setIsActive] = useState(true)
    const [show, setShow] = useState(false)
    const [modalObj, setModalObj] = useState([])
    const [starClass, setStarClass] = useState("icon_star white_star")

    useEffect(() => {
        const fav = JSON.parse(localStorage.getItem('Trip'))
        if(fav) {
            fav.forEach(obj => {
                if(obj.id === id) setStarClass("icon_star orange_star")
            })
        }
    }, [isActive])

    const showModal = e => {
        setShow(!show)
    }

    const openModal = ({target}) => {
        const modalID = modalsObj.find(item => item.id === target.id)
        setModalObj(modalID)
        setShow(true)
    }

    const addToFavorite = () => {
        if(starClass === "icon_star white_star") {
            setStarClass("icon_star orange_star")
        } else if(starClass === "icon_star orange_star") {
            setStarClass("icon_star white_star")
        }
        addToFav()
    }

    return (
        <>
            {close}
            <section className='section'>
                <img src={src} alt=""/>
                <div style={{position: 'relative'}}>
                    <p className='title'>{title}</p>
                    <p className='price'>Price: {price}</p>
                    <p className='article'>Article: {article}</p>
                    <FaStar onClick={addToFavorite} className={starClass}/>
                    <Button
                        id='modalID1'
                        className="btn"
                        text="Add to cart"
                        backgroundColor="red"
                        onClick={openModal}/>

                    {show && <Modal key={modalObj.id}
                                    modal={modalObj}
                                    addToBin={addToBin}
                                    onClose={showModal}
                                    show={show} />}
                </div>
            </section>
        </>
    )
}

export default Product;