import React from 'react';
import Product from "../components/Product";


const ProductList = ({ allList, close}) => {

    const addToFav = item  => {
        const fav = JSON.parse(localStorage.getItem("Trip"))
        if (fav) {
            const element = fav.find(elem => {
                return elem.id === item.id
            })
            const index = fav.indexOf(element)
            if (index === -1) {
                localStorage.setItem('Trip',JSON.stringify([...fav, item]))
            } else if (index!==-1) {
                fav.splice(index, 1)
                localStorage.setItem('Trip',JSON.stringify([...fav]))
            }
        } else {
            localStorage.setItem('Trip', JSON.stringify([item]))
        }
    }


    const addToBin = item => {
        const bin = JSON.parse(localStorage.getItem('TripBin'))
        if (bin) {
            const element = bin.find(elem => {
                return elem.id === item.id
            })
            const index = bin.indexOf(element)
            if (index === -1) {
                localStorage.setItem('TripBin',JSON.stringify([...bin, item]))
            }
        } else {
            localStorage.setItem('TripBin', JSON.stringify([item]))
        }
    }

    return (
            <section className='product_container'>
                {allList.map(item => (
                    <Product
                        key={item.id}
                        title={item.name}
                        article={item.article}
                        item={item}
                        src={item.img}
                        price={item.price}
                        addToFav={() => addToFav(item)}
                        id={item.id}
                        addToBin={() => addToBin(item)}
                        close={close}
                    />
                ))}
            </section>
        );
}

export default ProductList